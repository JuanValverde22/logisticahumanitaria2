package com.example.logisticahh;

import java.io.Serializable;

public class Estados implements Serializable {


private int _ID;
private String nombre;
private int estatus;
private String idMovil;

public Estados()
{
    this.set_ID(0);
    this.setNombre("");
    this.estatus = 0;
    this.idMovil="";
}

public Estados (int _ID, String nombre,int estatus,String idMovil)

{
    this.set_ID(_ID);
    this.setNombre(nombre);
    this.estatus = estatus;
    this.idMovil = idMovil;

}

    public int get_ID() {
        return _ID;
    }

    public String getIdMovil(){return idMovil;}

    public int getEstatus(){
        return  estatus;
    }

    public void set_ID(int _ID) {
        this._ID = _ID;
    }

    public void setIdMovil(String idMovil){this.idMovil = idMovil;}

    public void setEstatus(int estatus){
        this.estatus = estatus;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }
}
