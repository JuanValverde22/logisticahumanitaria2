package com.example.logisticahh;

import android.content.Context;
import android.util.Log;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONArray;
import org.json.JSONObject;
import java.util.ArrayList;

public class ProcesosPHP implements Response.Listener<JSONObject>,Response.ErrorListener {


    private RequestQueue request;
    private int res;
    private JsonObjectRequest jsonObjectRequest;
    Estados estados;
    private int idI;
    private ArrayList<Estados> contactos = new ArrayList<Estados>();

    private String serverip =
            "https://logisticahumanitaria2.000webhostapp.com/";

    public void setContext(Context context) {
        request = Volley.newRequestQueue(context);
    }

    public void consultarTodosWs(String dev) {
        int i = 0;
        try {

            String url = serverIp + "wsConsultarTodos.php?idMovil=" + dev;
            jsonObjectRequest = new JsonObjectRequest(Request.Method.GET, url, null, this, this);
            request.add(jsonObjectRequest);
            i = 1;

        } catch (Exception e) {
            i = 0;
        }
        ;
    }
    public void insertarEstadoWebService(final String nom, final String mov, final Context context)
    {
        idI = 0;
        String url = serverip + "wsRegistro.php?nombre="+ nom
                +"&idMovil="+ mov;
        url = url.replace(" ","%20");
        Log.e("ghhjg",""+url);
        jsonObjectRequest = new JsonObjectRequest(Request.Method.GET, url, null, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response)
            {

                JSONArray json = response.optJSONArray("resul");
                try
                {
                    JSONObject jsonObject = null;
                    jsonObject=json.getJSONObject(0);
                    idI = jsonObject.optInt("id");
                    Log.e("ID ID ID",""+ idI);
                    DBEstados source = new DBEstados(context);
                    source.openDataBase();
                    Estados nEstado = new Estados();
                    nEstado.set_ID(idI);
                    nEstado.setNombre(nom);
                    nEstado.setStatus(1);
                    nEstado.setIdMovil(mov);
                    source.insertEstado(nEstado);
                    source.closeDataBase();
                }
                catch (Exception e)
                {
                    Log.e("ERRORRRORORORORO:     ",""+ e.getMessage());
                }
            }
        }, this);
        request.add(jsonObjectRequest);
    }
    public int actualizarEstadoWebService(final Estados c, final Context context)
    {
        res = 0;
        String url = serverip + "wsActualizar.php?_ID="+c.get_ID()
                +"&nombre="+c.getNombre();
        url = url.replace(" ","%20");
        jsonObjectRequest = new JsonObjectRequest(Request.Method.GET, url, null, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response)
            {
                JSONArray json = response.optJSONArray("resp");
                try
                {
                    JSONObject jsonObject = null;
                    jsonObject=json.getJSONObject(0);
                    boolean id = jsonObject.optBoolean("respuesta");
                    Log.e("ID W:   ",""+ id);
                    DbEstados source = new DbEstados(context);
                    source.openDataBase();
                    Estados nEstado = new Estados();
                    nEstado.set_ID(c.get_ID());
                    nEstado.setNombre(c.getNombre());
                    nEstado.setStatus(1);
                    nEstado.setIdMovil(c.getIdMovil());
                    source.updateEstado(nEstado, c.get_ID());
                    source.closeDataBase();
                    Log.e("ACTULIZAR PHP:   ",""+ c.get_ID() + " mov: " + c.getIdMovil());


                }
                catch (Exception e)
                {
                    Log.e("ERRORRRORORORORO:     ",""+ e.getMessage());
                }


            }
        }, this);
        request.add(jsonObjectRequest);
        return  res;
    }

    public void borrarEstadoWebService(final int id, final Context context) {
        String url = serverip + "wsEliminar.php?_ID=" + id;
        jsonObjectRequest = new JsonObjectRequest(Request.Method.GET, url, null, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                JSONArray json = response.optJSONArray("resp");
                //deshabilitar
                try {
                    JSONObject jsonObject = null;
                    jsonObject = json.getJSONObject(0);
                    boolean re = jsonObject.optBoolean("res");
                    Log.e("RESPUESTAA BORRAR:   ", "" + re);
                    /*
                    DbEstados dbEstados = new DbEstados(context);
                    dbEstados.openDataBase();
                    long i = dbEstados.borrarEstados(id);
                    dbEstados.closeDataBase();
                    Log.e("ACTULIZAR PHP:   ",""+ i);*/


                } catch (Exception e) {
                    Log.e("ERRORRRORORORORO:     ", "" + e.getMessage());
                }
            }
        }, this);
        request.add(jsonObjectRequest);

        @Override
        public void onErrorResponse (VolleyError error){

        }

        @Override
        public void onResponse (JSONObject response){

        }
    }

    //Cosulta todos los estados registrados
    public void consultarTodosWebService(int id)
        {
            String url = serverip +"wsConsultarTodos.php=idMovil=" + id;
            jsonObjectRequest = new JsonObjectRequest(Request.Method.GET,url,null, this, this  );
            request.add(jsonObjectRequest);
        }
}
