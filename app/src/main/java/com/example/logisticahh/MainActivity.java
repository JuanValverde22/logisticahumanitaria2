package com.example.logisticahh;

import androidx.appcompat.app.AppCompatActivity;

import android.app.Activity;
import android.app.ListActivity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;

public class MainActivity extends AppCompatActivity implements Response.Listener<JSONObject>,Response.ErrorListener {


    Button Listar, guardar, limpiar;
    EditText edtNombre;
    private Estados savedContact;
    private int id;
    private final Context context = this;
    private ProcesosPHP php = new ProcesosPHP();
    private JsonObjectRequest jsonObjectRequest;
    private String oldEstado;
    private RequestQueue request;
    private String URL="https://logisticahh.000webhostapp.com/";

    private Integer Base;
    private Integer Base1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        try {
            setContentView(R.layout.activity_main);

            php.setContext(this);
            SharedPreferences prefs = getBaseContext().getSharedPreferences("vacio2",Context.MODE_PRIVATE);
            Base = prefs.getInt("id",0);

            if(Base == 0)
            {
                SharedPreferences pref1 = getSharedPreferences("vacio2",Context.MODE_PRIVATE);
                SharedPreferences.Editor editor = pref1.edit();
                editor.putInt("id",Base+1);
                editor.commit();
                try {

                    request = Volley.newRequestQueue(context);
                    php.setContext(MainActivity.this);
                    consultarTodos();

                }catch (Exception e)
                {

                };
            }


            edtNombre = (EditText) findViewById(R.id.edtNombre);
            Listar = (Button) findViewById(R.id.btnListar);
            guardar = (Button) findViewById(R.id.btnGuardar);
            limpiar = (Button) findViewById(R.id.btnLimpiar);


            Listar.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Intent intent = new Intent(MainActivity.this, ListaActivity.class);
                    startActivityForResult(intent, 0);
                }
            });

            if(isOnline(getApplicationContext()))
            {
                guardar.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        boolean completo = true;
                        if (edtNombre.getText().toString().equals("")) {
                            edtNombre.setError("Introduce el nombre");
                            completo = false;
                        }
                        if (completo) {
                            DBEstados source = new DBEstados(MainActivity.this);
                            source.openDatabase();
                            Estados nContacto = new Estados();
                            nContacto.setNombre(edtNombre.getText().toString());
                            nContacto.setEstatus(1);
                            if (savedContact == null) {
                                source.insertContacto(nContacto);
                                Toast.makeText(MainActivity.this, R.string.mensaje,
                                        Toast.LENGTH_SHORT).show();
                                limpiar();
                            } else {
                                source.updateContacto(nContacto, id);
                                Toast.makeText(MainActivity.this, R.string.mensajeedit,
                                        Toast.LENGTH_SHORT).show();
                                limpiar();
                            }
                            source.close();

                        }
                    }
                });
            }

            limpiar.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    savedContact = null;
                    edtNombre.setText("");
                    edtNombre.requestFocus();
                }
            });

        } catch (Exception e) {
            Toast.makeText(MainActivity.this, e.getMessage(), Toast.LENGTH_SHORT).show();
        }


    }

    private void consultarTodos() {

        String dev = Device.getSecureId(this);
        Toast.makeText(getApplicationContext(),"DEV = " + dev,Toast.LENGTH_SHORT).show();
        try{

            String url = URL + "wsConsultarTodos.php?idMovil=" + dev;
            jsonObjectRequest = new JsonObjectRequest(Request.Method.GET,url,null,this,this);
            request.add(jsonObjectRequest);

        }catch (Exception e)
        {
            Toast.makeText(getApplicationContext(),e.getMessage(),Toast.LENGTH_SHORT).show();
        };
    }

    public void limpiar() {
        savedContact = null;
        edtNombre.setText("");
        edtNombre.requestFocus();
    }

    protected void onActivityResult(int requestCode, int resultCode,Intent data)
    {
        if (Activity.RESULT_OK == resultCode)
        {
            Estados contacto = (Estados) data.getSerializableExtra("estado");
            savedContact = contacto;
            id = contacto.get_ID();
            edtNombre.setText(contacto.getNombre());
        }
        else
        {
            limpiar();
        }
    }

    protected static boolean isOnline(Context context)
    {
        ConnectivityManager connectivityManager = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo networkInfo = connectivityManager.getActiveNetworkInfo();
        return networkInfo != null && networkInfo.isAvailable() && networkInfo.isConnected();
    }

    @Override
    public void onErrorResponse(VolleyError error) {

        error.printStackTrace();
        Toast.makeText(getApplicationContext(),"Error en el Volley aki: "+ error.getMessage(),Toast.LENGTH_SHORT).show();

    }

    @Override
    public void onResponse(JSONObject response) {

        SharedPreferences pref = getBaseContext().getSharedPreferences("vacio5",Context.MODE_PRIVATE);
        Base1 = pref.getInt("id",0);

        if(Base == 0)
        {
            SharedPreferences pref2 = getSharedPreferences("vacio5",Context.MODE_PRIVATE);
            SharedPreferences.Editor editor = pref2.edit();
            editor.putInt("id",Base+1);
            editor.commit();

            Estados estados = null;

            try{

                JSONArray json = response.optJSONArray("Estados");
                try{

                    for(int i=0;i<json.length();i++)
                    {
                        estados = new Estados();
                        JSONObject jsonObject = null;
                        jsonObject = json.getJSONObject(i);
                        estados.set_ID(jsonObject.optInt("id"));
                        estados.setNombre(jsonObject.optString("nombre"));
                        estados.setIdMovil(jsonObject.optString("idMovil"));
                        DBEstados source = new DBEstados(MainActivity.this);
                        source.openDatabase();
                        source.insertContacto(estados);
                        source.close();
                    }

                }catch (Exception e)
                {

                };

            }catch (Exception e)
            {

            };
        }

    }
}

