package com.example.logisticahh;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

import java.util.ArrayList;

public class DBEstados  {

    Context context;
    EstadoAdapter mDbHelper;
    SQLiteDatabase db;
    String[] columnsToRead = new String[]
    {
        DefinirTabla.Contacto._ID,
        DefinirTabla.Contacto.COLUMN_NAME_NOMBRE,
        DefinirTabla.Contacto.COLUMN_STATUS
    };

    public DBEstados(Context context)
    {
        this.context = context;
        mDbHelper = new EstadoAdapter(this.context);
    }
    public void openDatabase()
    {
        db = mDbHelper.getWritableDatabase();
    }

    public long insertContacto(Estados c)
    {
        ContentValues values = new ContentValues();
        values.put(DefinirTabla.Contacto.COLUMN_NAME_NOMBRE, c.getNombre());
        values.put(DefinirTabla.Contacto.COLUMN_STATUS, c.getEstatus());
        return db.insert(DefinirTabla.Contacto.TABLE_NAME, null, values);
        //regresa el id insertado
    }

    public long updateContacto(Estados c,int id)
    {
        ContentValues values = new ContentValues();
        values.put(DefinirTabla.Contacto.COLUMN_NAME_NOMBRE, c.getNombre());
        Log.d("values", values.toString());
        return db.update(DefinirTabla.Contacto.TABLE_NAME , values,
                DefinirTabla.Contacto._ID + " = " + id,null);
    }

    public int deleteContacto(long id)
    {
        ContentValues values = new ContentValues();
        values.put(DefinirTabla.Contacto.COLUMN_STATUS, 0);
        Log.d("values", values.toString());
        return db.delete(DefinirTabla.Contacto.TABLE_NAME, "_ID = ?", new String[]{String.valueOf(id)});
        //return db.update(DefinirTabla.Contacto.TABLE_NAME , values, DefinirTabla.Contacto._ID + " = ?" ,new String[]{String.valueOf(id)});
    }

    private Estados readContacto(Cursor cursor){
        Estados c = new Estados();
        c.set_ID(cursor.getInt(0));
        c.setNombre(cursor.getString(1));
        c.setEstatus(cursor.getInt(2));
        return c;
    }

    public Estados getContacto(long id){
        SQLiteDatabase db = mDbHelper.getReadableDatabase();
        Cursor c = db.query(DefinirTabla.Contacto.TABLE_NAME,
                columnsToRead,
                DefinirTabla.Contacto.COLUMN_STATUS + " = ?",
                new String[]{"1"},
                null,
                null,
                null
        );
        c.moveToFirst();
        Estados contacto = readContacto(c);
        c.close();
        return contacto;
    }

    public ArrayList<Estados> allContactos()
    {
        Cursor cursor = db.query(DefinirTabla.Contacto.TABLE_NAME, columnsToRead, null, null, null, null, null);
        ArrayList<Estados> contactos = new ArrayList<Estados>();
        cursor.moveToFirst();
        while(!cursor.isAfterLast()){
            Estados c = readContacto(cursor);
            contactos.add(c);
            cursor.moveToNext();
        }
        cursor.close();
        return contactos;
    }
    public void close()
    {
        mDbHelper.close();
    }

    public void insertEstado(Estados nEstado) {
    }
}
