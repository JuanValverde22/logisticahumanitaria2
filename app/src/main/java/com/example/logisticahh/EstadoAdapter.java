package com.example.logisticahh;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

public class EstadoAdapter extends SQLiteOpenHelper {

    private static final String TEXT_TYPE = " TEXT";
    private static final String INTEGER_TYPE = " INTEGER";
    private static final String COMMA_SEP = ",";
    private static final String SQL_CREATE_CONTACTO = "CREATE TABLE " +
            DefinirTabla.Contacto.TABLE_NAME + " ("+
            DefinirTabla.Contacto._ID + " INTEGER PRIMARY KEY AUTOINCREMENT, "+
            DefinirTabla.Contacto.COLUMN_NAME_NOMBRE + TEXT_TYPE  +
            COMMA_SEP +
            DefinirTabla.Contacto.COLUMN_STATUS + " INTEGER" +
            ")";
    private static final String SQL_DELETE_CONTACTO = "DROP TABLE IF EXISTS " + DefinirTabla.Contacto.TABLE_NAME;
    public static final int DATABASE_VERSION = 1;
    public static final String DATABASE_NAME = "estados2.db";

    public EstadoAdapter(Context context)
    {
        super(context,DATABASE_NAME,null,DATABASE_VERSION);
    }


    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL(SQL_CREATE_CONTACTO);
    }

    public void onDowngrade(SQLiteDatabase db, int oldVersion, int
            newVersion){
        onUpgrade(db, oldVersion, newVersion);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL(SQL_DELETE_CONTACTO);
        onCreate(db);
    }
}
